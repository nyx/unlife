# Unlife

This is the source for my blog, with some (pretty bad) code in `src` for
generating the pages with Common Lisp. The posts themselves are currently
(unfortunately) imprisoned in GNU Emacs org-mode files and are generated through
the Emacs tools.

Code is licensed under ISC. See my writings repo for the licensing for the posts
themselves.

#!/usr/bin/env bash
# set -euo pipefail

arg1="$1"
arg2="$2"

make_tar () {
    tar -f - -C public \
        --exclude="static/fonts/3270" \
        --exclude="static/fonts/txlibera" \
        -cvz . > site.tar.gz
}

post_to_srht () {
    curl --oauth2-bearer "$arg2" -Fcontent=@site.tar.gz https://pages.sr.ht/publish/unlife.nyx.land
}

main () {
    case "$arg1" in
        "-tar")
            make_tar
            ;;
        "-post")
            post_to_srht
            ;;
        "-publish")
            make_tar
            post_to_srht
            ;;
        *)
        echo "Pass -tar, -post, or -publish to this script"
        echo
        exit 1
        ;;
    esac
}

main

(defvar *months*
  '(("Jan" . 1)
    ("Feb" . 2)
    ("Mar" . 3)
    ("Apr" . 4)
    ("May" . 5)
    ("Jun" . 6)
    ("Jul" . 7)
    ("Aug" . 8)
    ("Sep" . 9)
    ("Oct" . 10)
    ("Nov" . 11)
    ("Dec" . 12)))

(defun get-posts (dir &optional rm)
  (let ((posts 
          (directory
           (merge-pathnames
            (make-pathname
             :directory '(:relative :wild-inferiors)
             :name :wild
             :type "html")
            dir))))
    (if rm (remove-nth rm posts) posts)))

(defun encode-time (datestr)
  (let ((c (subseq datestr (1+ (position #\space datestr)))))
    (local-time:encode-timestamp
     0 0 0 0
     (parse-integer (subseq c 4) :junk-allowed t)
     (cdr (assoc (subseq c 0 3) *months* :test #'equalp))
     (parse-integer (subseq c (+ 2 (position #\, c)))))))

(defun parse-date (node)
  (flet ((remove-junk (c)
           (or (eq c #\newline)
               (eq c #\tab))))
    (let* ((meta-text
             (remove-if #'remove-junk
                        (lquery:$1 node ".post-bottom-div"
                          (text))))
           (substr "n1xon") ;; lol
           (index (+ (length substr)
                     (search substr meta-text))))
      (string-trim '(#\space)
                   (subseq meta-text index)))))

(defun parse-title (node)
  (let ((meta-text
          (lquery:$1 node "title" (text))))
    (subseq meta-text 0 (1- (position #\| meta-text)))))

(defun pandoc-convert (node)
  (uiop:with-temporary-file (:stream str
                             :pathname path)
    (let ((org-path (make-pathname
                     :directory (pathname-directory path)
                     :name (file-namestring path)
                     :type "org")))
      (plump:serialize
       (lquery:$1 node ".post-content")
       str)
      (uiop:run-program
       (format nil "pandoc ~a -o ~a"
               path org-path))
      (alexandria:read-file-into-string org-path)))
  ;; (let ((out-name (format nil "~a.org"
  ;;                         (car (last (pathname-directory post))))))
  ;;   (uiop:run-program
  ;;    (format nil "pandoc ~aindex.html -o ~a~a"
  ;;            (directory-namestring post)
  ;;            output
  ;;            out-name)))
  )

(defun write-orgs (path node out)
  (let* ((date (encode-time (parse-date node)))
         (title (parse-title node))
         (export-name (car (last (pathname-directory path)))))
    (with-open-file (str out
                         :if-does-not-exist :create
                         :if-exists :append)
      (format str "* DONE ~a
CLOSED: [~a 00:00]
:PROPERTIES:
:EXPORT_FILE_NAME: ~a
:END:~%~%"))))

(defun main (posts out)
  (loop for p in posts
        as node = (plump:parse p)
        do (write-orgs p node out))
  ;;(sort (copy-seq meta) #'local-time:timestamp> :key #'cdr)
  )

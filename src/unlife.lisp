(in-package #:unlife)

(defparameter *org-file*
  #P"~/docs/org-sync/blogs/unlife.org")

(defparameter *page-list*
  (make-hash-table :test #'equalp))

(defvar *gpg-key*
"-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEYsNyMxYJKwYBBAHaRw8BAQdAyWnSk+P92r2TDosUb1ZqCHWdx+BA2kBcXGW6
zoWSS6q0GU55eCBMYW5kIDxuMXhAcmlzZXVwLm5ldD6IlAQTFgoAPBYhBBLTp+6h
PPUUU+js6rUPt2N3EVXfBQJiw3IzAhsDBQsJCAcCAyICAQYVCgkICwIEFgIDAQIe
BwIXgAAKCRC1D7djdxFV3w9VAP9116dsn4hDhq+eYD2Aqnb3XCxA069hVal/r4/R
yRcLPwEAj/9G9/M0C84a02hdIcwtVPscsAZOAsx6vhdlwKgagge4OARiw3IzEgor
BgEEAZdVAQUBAQdAUTWzyh7A+BrrBvM3QwqCnJqoInxurwlC/97Ku1bMsF4DAQgH
iHgEGBYKACAWIQQS06fuoTz1FFPo7Oq1D7djdxFV3wUCYsNyMwIbDAAKCRC1D7dj
dxFV3wseAPoCg0cGBGjjQklDHtc5vMEx9unfNsEnR0+Ht+4DWPuZRAD+O8VfCFV5
x07rLl42gWv/fvT8gVn8/PGXVFc/UeL5swE=
=NBVY
-----END PGP PUBLIC KEY BLOCK-----
")

(defvar *site-title* "Unlife")
(defvar *site-subheading* "Null utterances from a damaged and deranged mind")

(defvar *footer*
  '(:div :class "footer"
    (:p
     (:span :style "display:inline-block;transform:rotate(180deg);" "©")
     (:a :href "https://codeberg.org/nyx/writings" "Anti-Copyright 20XX n1x, Nyx Land, et. al."))
    (:a :href "https://nyx.land"
     (:img :id "syzygy" :src "/static/svg/90.svg"))))

(defun make-header (title subheading)
  `(:div :class "header"
    (:div :class "site-title"
          (:h1 :class "header-heading" (who:esc ,title))
          (:h2 :class "header-subheading"
               :style "line-height:2.5em;"
               (:i (who:esc ,subheading))))
    (:div :class "site-logo"
          (:ul
           (:li "n1x [at] [riseup] [dot] net (<a href='/gpg.html'>GPG</a>)"
                ;;(:a :href "/gpg.html" "GPG")
                )
           (:li (:a :href "https://social.xenofem.me/nyx"
                    "@nyx@social.xenofem.me"))
           (:li (:a :href "https://twitter.com/nyx_land______"
                    "@nyx_land______@twitter.com"))
           (:li (:a :href "/donate.html"
                    "Donate XMR")))
          (:img :id "logo" :src "/static/svg/sigil.svg"))))

(defmacro defpage ((&key (title *site-title*) (subheading *site-subheading*)
                      path (header t) (footer *footer*))
                   &body body)
  `(setf (gethash ,path ,*page-list*)
         (who:with-html-output-to-string (*standard-output* nil
                                          :prologue t :indent t)
           (:html
            (:head
             (:link :rel "stylesheet" :href "/static/css/style.css")
             (:meta :name "viewport" :content "width=device-width, initial-scale=1.0")
             (:title ,title))
            ,@(when header
                (list
                 (make-header title subheading)
                 '(:hr)))
            (:div :class "wrapper"
                  ,@body
                  ,footer
                  (:img :id "unlife-banner"
                        :src "/static/img/web-banner-88x32.png")

                  (:a :href "https://decolonizepalestine.com"
                      (:imd :id "palestine-banner"
                            :src "/static/img/free-palestine-banner.png")))))))

(defun export-html (&key (output-dir #P"public/"))
  (let ((base-dir
          (merge-pathnames output-dir (asdf:system-source-directory :unlife))))
    (loop for k being the hash-key
            using (hash-value v) of *page-list*
          as path = (merge-pathnames k base-dir)
          do (ensure-directories-exist path)
             (with-open-file (str path
                                  :direction :output
                                  :if-does-not-exist :create
                                  :if-exists :supersede)
               (format str v)))))

(defpage (:title "Unlife" :path "index.html")
  (:div (:a :href "/films/index.html" "[spectacle]"))
  (:div
   (:ul (loop for post in (filter-posts
                           (collect-posts *org-file*)
                           (lambda (path)
                             (not (member "films" (pathname-directory path) :test #'equalp))))
              do (who:htm
                  (list-post post))))))

(defpage (:title "Spectacle"
          :subheading "My thoughts have been replaced by moving images"
          :path "films/index.html")
  (:div (:a :href "/index.html" "[unlife]"))
  (:div :id "content" :class "content"
        (:p "In the style of"
            (:a :href "http://www.thelastexit.net/cinema/" "The Worldwide Celluloid Massacre")
            ", I decided I will start a
little movie blog where I try to condense my thoughts on movies I watch into a
few paragraphs at most. I watch a lot of movies and like film, and occasionally
if I really like a movie I will write a full-length essay about it, but
sometimes I just want to put down my thoughts on something and be done with
it. ")
        (:p "These posts will use a RedLetterMedia style “rating” system: A simple
“recommended” or “not recommended” judgement, for any reason that makes a movie
worth watching. This means it may not necessarily be a" (:i "good") "movie. These are
also just my own opinions and may differ from the reader. ")
        (:ul (loop for post in (filter-posts
                                (collect-posts *org-file*)
                                (lambda (path)
                                  (member "films" (pathname-directory path) :test #'equalp)))
                   do (who:htm
                       (list-post post))))))

(defpage (:title "Donate"
          :path "donate.html"
          :header nil
          :footer nil)
  (:div
   (:pre "48cLnDnZ3X14w9qh5YbPyzeLF6irfrzwd7PimZhp6QfT3kVrk1rhSdT1idCPb3VuYi4dB3sno2viMTzWHCxpcgwH9ndh6qf")
   (:img :src "/static/img/moneroqr.png")))

(defpage (:title "GPG key"
          :path "gpg.html"
          :header nil
          :footer nil)
  (:pre (who:esc *gpg-key*)))

(defpage (:title "Gackcore -- A Style Guide"
          :title "gackcore.html"
          :header nil
          :footer nil)
  (:p "Over the years, various people have apparently taken note of and
imitated my style of web design -- what you might call the \"Gackcore
Aesthetic™\". It's common for people to formalize a set of guidelines
for a design aesthetic, and while I am no designer, I do have a
certain particular aesthetic sense that I am obviously fond of being
consistent about. This overall style goes back to my"
      (:a :href "https://b0x.neocities.org" " old neocities site ")
      "and ever since then I have continued to develop on it. You'll note
that things have changed quite a bit since then, but the core ideas
remain the same.")
  (:p "This isn't intended to be a set of strict rules to follow, but is more
meant to describe the historical and philosophical interests that have
informed my aesthetic design sensibilities as well as its
implementation in HTML and CSS. This means it takes the form of both
an essay and a design document. This is, however, only a single
implementation, and you are encouraged to tweak it and make it your
own. But I also will not complain if you just copy it outright.")
  (:p "That last point bears expanding on.")
  (:h2 "Platform Capitalism's Colonialism of Aesthetics")
  (:p "The concept of design aesthetics
has caught on in recent years in internet culture as the halcyon days
of Web 1.0 hyper-individualistic and maximalist design, what we now
call the 'retro web', were replaced by the homogenized, gentrified
even, Web 2.0 design aesthetics of platform capitalist social
software. This isn't limited to web design, but also to internet
subcultures like vaporwave and witch house that spearheaded a
rennaissance in thinking about digital spaces as a medium for
individualistic expression. Google's Material design and Facebook's
color palette had a lot to do with what the web started to look like
in the late 2000s and 2010s. If the old web was hyper-individualistic
and maximalist, then the new web eschewed the skeumorphistic
aesethetics of the old web and flattened it out both literally and
conceptually. For a period of time, everything seemed to resemble
Facebook and Google products: Flat, rounded, muted colors, large
amounts of whitespace.")
  (:p "Calling this a 'Colonialism of Aesthetics' isn't a superficial comparison to colonialism but is rather intentionally a reference to a new theory of digital colonialism:"
      (:a :href "https://www.sup.org/books/title/?id=28816"
          " The Costs of Connection: How Data Is Colonizing Human Life and
Appropriating It for Capitalism")
      ". It would be far beyond the scope of this post to discuss the theory
in depth, and at the time of this version of the document (Thu Feb 15
01:59:55 PST 2024) I also haven't finished the book. But I will say
briefly that the central argument of the book is that the concept of
digital colonialism is based in a Marxist analysis of capitalism's
tendency towards infinite expansion and abstraction/alienation that
has evolved to be able to appropriate the very stuff of human life
itself. A huge amount of our lives beginning in the 2010s began to be
mediated predominantly by Silicon Valley 'platforms' like Facebook,
Twitter, YouTube, etc. which, as the book describes, have a vast array
of 'sensors' to capture as much data as possible about the people
using these spaces in order to extract profit from it. As the old
saying goes, if the product is free, then it's you who are the
product.")
  (:p "My interest in creating a semi-formalized idea of what Gackcore is comes from the tendency that "))

(in-package #:unlife)

(defstruct post title date path)

(defun new-path (full-path)
  (let ((rel-dir (push :absolute
                       (cdr (member "public"
                                    (pathname-directory full-path)
                                    :test #'equalp)))))
    (make-pathname
     :directory rel-dir
     :name (pathname-name full-path)
     :type "html")))

(defun parse-path (line)
  (let ((full-path
          (parse-namestring
           (subseq line (+ 2 (search ": " line))))))
    (new-path full-path)))

(defun parse-date (line)
  (let* ((cleaned (remove #\] (remove #\[ line)))
         (spc1 (position #\space cleaned))
         (spc2 (position #\space cleaned :start (1+ spc1))))
    (cl-date-time-parser:parse-date-time
     (concatenate
      'string
      (subseq cleaned 0 spc1)
      (subseq cleaned spc2)))))

(defun post-parser (heading stream)
  (let ((post (make-post))
        (closed (read-line stream)))
    (setf (post-title post)
          (subseq heading
                  (+ 5 (search "DONE" heading))
                  (search " :" heading)))
    (setf (post-date post)
          (subseq closed (1+ (search " [" closed))))
    (loop for line = (read-line stream)
          until (search ":END:" line)
          when (search "EXPORT_FILE_NAME" line)
            do (setf (post-path post)
                     (parse-path line)))
    (cons (parse-date (post-date post)) post)))

(defun collect-posts (file)
  (with-open-file (org file)
    (loop for line = (read-line org nil :eof)
          until (eq line :eof)
          when (search "* DONE" line)
            collect (post-parser line org) into posts
          finally (return (sort (copy-seq posts) #'> :key #'car)))))

(defun list-post (post)
  (let ((post (cdr post)))
    (who:with-html-output (*standard-output* nil :indent t)
      (:li (:a :href (namestring
                      (post-path post))
               (format t "~a - ~a"
                       (post-date post)
                       (post-title post)))))))

(defun filter-posts (posts filter-cond)
  (loop for p in posts
        as path = (post-path (cdr p))
        when (funcall filter-cond path)
          collect p))

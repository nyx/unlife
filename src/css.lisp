(in-package :unlife)

;;;; LASS customizations to declare multiple formats and sources for fonts
(defparameter *font-names*
  '((".ttf" . "truetype")
    (".woff" . "woff")
    (".woff2" . "woff2")
    (".otf" . "opentype")
    (".svg" . "svg")))

;; TODO: this needs to lookup the font type
(defun get-font-format (path)
  (let ((sel (subseq path (search "." path))))
    (gethash (subseq sel 0 (search "\"" sel))
             *font-names*)))

(defun return-font-str (font)
  (let ((fontstr (lass:resolve font)))
    (concatenate 'string
                 fontstr
                 (format nil " format(~s)" (get-font-format fontstr)))))

(lass:define-special-property src (&rest forms)
  (list
   (lass:make-property
    "src"
    (format nil "~{~a~^,~%~}"
            (mapcar #'return-font-str forms)))))

(defun font-temp (name &rest paths)
  `(:font-face
    :font-family ,name
    :src ',(mapcar (lambda (x) (url x))
                   paths)))

(defparameter colors
  '((:main . "#E60073")
    (:accent . "#D15CFF")
    (:background . "#000000")))

(defparameter fonts
  '((:header . "Textura-Libera-Black")
    (:subheader . "Textura-Libera-Condensed-Thin")
    (:subsubheader . "IBM-3270")
    (:body . "IBM-3270")))

(defparameter static
  '((:fonts . ((:txblk . "../fonts/TexturaLiberaX-Black.otf")
               (:txthin . "../fonts/TexturaLiberaC-Thin.otf")
               (:ibm . '("../fonts/3270-Regular.otf"
                         "../fonts/3270-Regular.woff"))))))

(defparameter css/fonts
  `(,(font-temp "Textura-Libera-Black"
                "../fonts/TexturaLiberaX-Black.otf")
    ,(font-temp "Textura-Libera-Condensed-Thin"
                "../fonts/TexturaLiberaC-Thin.otf")
    ,(font-temp "IBM-3270"
                "../fonts/3270-Regular.otf")))

(defparameter css/global
  `((hr
     :color ,(cdr (assoc :main colors)))
    (body
     :background ,(cdr (assoc :background colors))
     :font-family ,(cdr (assoc :body fonts))
     :color ,(cdr (assoc :main colors))
     :max-width "900px")
    (a
     :text-decoration "none"
     :color ,(cdr (assoc :accent colors)))
    (ul
     :list-style-type "none"
     :padding-left "0px")
    ((:or h1 h2 h3 h4 h5 h6)
     :line-height "10px")))

(defparameter css/classes
  `((.title
     :font-family "Textura-Libera-Black"
     :font-size "70px"
     :padding-bottom "50px"
     :text-align "left")
    (.header
     :line-height "2.5em"
     (.header-heading
      :padding-bottom "1px"
      :font-size "120px"
      :color ,(cdr (assoc :main colors))
      :font-family ,(cdr (assoc :header fonts)))
     (.header-subheading
      :color ,(cdr (assoc :main colors))
      :font-family ,(cdr (assoc :subheader fonts))))
    (.footer
     :text-align "left"
     (.footer-meta
      :position "fixed"
      :font-size "13px"
      :bottom "5%")
     (.footer-img
      :position "fixed"
      :bottom "0%"))))

(defun css-sheet ()
  (apply 'lass:compile-and-write
         (append css/fonts
                 css/global
                 css/classes)))

(defparameter css
  `((:file . #P"public/static/css/lass.css")
    (:css . ,(css-sheet))))

(defun css->file ()
  (with-open-file (out (cdr (assoc :file css))
                       :direction :output
                       :if-exists :supersede
                       :if-does-not-exist :create)
    (write-sequence (cdr (assoc :css css)) out)))

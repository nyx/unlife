(defsystem "unlife"
  :author "Nyx <n1x@riseup.net>"
  :license "ISC"
  :description "an absolute madwoman's blog"
  :depends-on ("cl-date-time-parser"
               "cl-who"
               "lass")
  :components ((:module "src"
               :components
               ((:file "package")
                (:file "org")
                (:file "unlife")
                ;;(:file "css")
                ))))
